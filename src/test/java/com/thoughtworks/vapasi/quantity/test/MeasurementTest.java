package com.thoughtworks.vapasi.quantity.test;

import com.thoughtworks.vapasi.quantity.Measurement;
import com.thoughtworks.vapasi.quantity.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MeasurementTest {
    @Test
    void shouldCheckForEqualityOfMetersAndCentimetersMeasurements() {
        assertEquals(Measurement.meter(1), Measurement.centimeter(100));
    }

    @Test
    void shouldCheckIfMetersAndCentimetersMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.meter(2), Measurement.centimeter(300));
    }

    @Test
    void shouldCheckForEqualityOfMetersAndKiloMetersMeasurements() {
        assertEquals(Measurement.meter(1000), Measurement.kilometer(1));
    }

    @Test
    void shouldCheckIfMetersAndKilometerMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.meter(1000), Measurement.kilometer(2));
    }

    @Test
    void shouldCheckForEqualityOfGramsAndKiloGramsMeasurements() {
        assertEquals(Measurement.grams(1000), Measurement.kiloGrams(1));
    }

    @Test
    void shouldCheckIfGramsAndKiloGramsMeasurementsAreNotEqual() {
        assertNotEquals(Measurement.grams(2000), Measurement.kiloGrams(1));
    }

}
