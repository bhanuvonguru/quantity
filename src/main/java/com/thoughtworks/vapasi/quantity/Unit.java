package com.thoughtworks.vapasi.quantity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Unit {
    private String name;
    private String type;
    private Map<String, Double> conversionMap = new HashMap<String, Double>() {
        {
            put(QuantityConstants.METER, QuantityConstants.CONVERSION_FACTOR_METER);
            put(QuantityConstants.CENTIMETER, QuantityConstants.CONVERSION_FACTOR_CENTIMETER);
            put(QuantityConstants.KILOMETER, QuantityConstants.CONVERSION_FACTOR_KILOMETER);
            put(QuantityConstants.GRAM, QuantityConstants.CONVERSION_FACTOR_GRAM);
            put(QuantityConstants.KILOGRAM, QuantityConstants.CONVERSION_FACTOR_KILOGRAM);

        }
    };

    public Unit() {

    }

    public Unit(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public double convertToBaseValue(Unit unit) {
        return conversionMap.get(unit.name);
    }

    public static boolean checkIfTypeIsSame(Unit firstUnit, Unit secondUnit) {
        return (firstUnit.type).equalsIgnoreCase(secondUnit.type);
    }

}
