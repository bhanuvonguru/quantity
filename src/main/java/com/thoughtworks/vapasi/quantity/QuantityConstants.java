package com.thoughtworks.vapasi.quantity;

public class QuantityConstants {
    public static final String METER = "meter";
    public static final String CENTIMETER = "centimeter";
    public static final String KILOMETER = "kilometer";
    public static final String GRAM = "gram";
    public static final String KILOGRAM = "kilogram";
    public static final double CONVERSION_FACTOR_METER = 1;
    public static final double CONVERSION_FACTOR_CENTIMETER = 0.01;
    public static final double CONVERSION_FACTOR_KILOMETER = 1000;
    public static final double CONVERSION_FACTOR_GRAM = 1;
    public static final double CONVERSION_FACTOR_KILOGRAM = 1000;
}
