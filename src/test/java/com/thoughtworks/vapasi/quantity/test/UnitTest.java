package com.thoughtworks.vapasi.quantity.test;

import com.thoughtworks.vapasi.quantity.Unit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {

    @Test
    void shouldCheckIfTypeIsSame() {
        assertTrue(Unit.checkIfTypeIsSame(new Unit("meter", "length"), new Unit("kilometer", "length")));
    }

    @Test
    void shouldCheckIfTypeIsNotSame() {
        assertFalse(Unit.checkIfTypeIsSame(new Unit("gram", "weight"), new Unit("kilometer", "length")));
    }

    @Test
    void shouldCheckForEqualityOfConversionToBaseValue() {
        Unit unit = new Unit();
        assertEquals(unit.convertToBaseValue(new Unit("meter", "length")
        ), unit.convertToBaseValue(new Unit("meter", "length")));
    }

    @Test
    void shouldCheckIfConversionToBaseValueIsNotEqual() {
        Unit unit = new Unit();
        assertNotEquals(unit.convertToBaseValue(new Unit("meter", "length")
        ), unit.convertToBaseValue(new Unit("kilometer", "length")));
    }

}
