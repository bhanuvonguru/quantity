package com.thoughtworks.vapasi.quantity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Measurement {
    private int value;
    private Unit unit;


    public Measurement(int value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public static Measurement kilometer(int value) {
        Measurement measurement = new Measurement(value, new Unit("kilometer", "length"));
        return measurement;
    }

    public static Measurement meter(int value) {
        Measurement measurement = new Measurement(value, new Unit("meter", "length"));
        return measurement;
    }

    public static Measurement centimeter(int value) {
        Measurement measurement = new Measurement(value, new Unit("centimeter", "length"));
        return measurement;
    }

    public static Measurement grams(int value) {
        Measurement measurement = new Measurement(value, new Unit("gram", "weight"));
        return measurement;
    }

    public static Measurement kiloGrams(int value) {
        Measurement measurement = new Measurement(value, new Unit("kilogram", "weight"));
        return measurement;
    }

    @Override
    public boolean equals(Object o) {
        double conversionFactorForFirstUnit = 0.0;
        double conversionFactorForSecondUnit = 1.0;
        if (Unit.checkIfTypeIsSame(this.unit, ((Measurement) o).unit)) {
            conversionFactorForFirstUnit = unit.convertToBaseValue(this.unit);
            conversionFactorForSecondUnit = unit.convertToBaseValue(((Measurement) o).unit);
        }
        return Double.compare((this.value) * conversionFactorForFirstUnit,
                (((Measurement) o).value) * conversionFactorForSecondUnit) == 0;
    }
}
